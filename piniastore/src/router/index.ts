import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Vistaview from '../views/VistaView.vue'
import CreateView from '@/views/AddAlumno.vue'
import AlumnoView from '@/views/AlumnoView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
     
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/vista',
      name: 'vista',
      component: Vistaview
    },
    {
      path: '/create',
      name: 'create',
      component: CreateView
    },
    {
      path: '/aview/:id',
      name: 'aview',
      component: AlumnoView
    },
  ]
})

export default router

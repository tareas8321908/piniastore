import { ref, computed, type Ref } from 'vue'
import { defineStore } from 'pinia'
import AlumnoService from '@/services/AlumnosService';
import type { IUser } from '@/interface/IUser';

export const useStore = defineStore('alumno', () => {
    
    const service = ref(new AlumnoService());
    const alumnos = ref([]) as Ref<IUser[]>
    const alumno = ref({}) as Ref<IUser>

   
    async function getAlumnos() {
        await service.value.fetchAll();
        alumnos.value = service.value.getalumnos().value;
    }
    async function VerAlumno(id: number) {
        await service.value.fetchAlumno(id);
        alumno.value = service.value.getalumno().value;
    }
    async function postAlumno(name: string, email: string, group: string) {
        await service.value.CreateStudent(name, email, group);
        alumnos.value = service.value.getalumnos().value;
    }
    async function deleteAlumno(id: number) {
        await service.value.DeleteStudent(id);
        await service.value.fetchAll();
        alumnos.value = service.value.getalumnos().value;
    }

    
    return { alumnos, alumno, getAlumnos, VerAlumno, postAlumno, deleteAlumno,}

});

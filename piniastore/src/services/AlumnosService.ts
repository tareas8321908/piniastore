import { ref } from 'vue'
import type { Ref } from 'vue'
import type { IUser } from '@/interface/IUser'
import { defineStore } from 'pinia'
import type { NumericLiteral } from 'typescript'

const url =
  import.meta.env.VITE_API_URL || 'https://65e8dab54bb72f0a9c508303.mockapi.io/dev/apa/Alumnos'

export default class UserService {
  private Alumno: Ref<IUser>
  private Alumnos: Ref<Array<IUser>>

  constructor() {
    this.Alumno = ref({}) as Ref<IUser>
    this.Alumnos = ref<Array<IUser>>([])
  }

  getalumnos(): Ref<Array<IUser>> {
    return this.Alumnos
  }

  getalumno(): Ref<IUser> {
    return this.Alumno
  }

  async fetchAll(): Promise<void> {
    try {
      const response = await fetch(url)
      const json = await response.json()
      this.Alumnos.value = await json
    } catch (error) {
      console.log(error)
    }
  }

  async fetchAlumno(id: number): Promise<void> {
    try {
      const json = await fetch(url +'/'+ id)
      const response = await json.json()
      this.Alumno.value = await response
    } catch (error) {
      console.log(error)
    }
  }

  async CreateStudent(name: string, email: string, group: string): Promise<void> {
    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 'name': name, 'email': email, 'group': group })
      })

      const responseData = await response.json()
      console.log(responseData)

    } catch (error) {
      console.log(error)
    }
  }

  async DeleteStudent(id: number): Promise<void> {
    try {
        const json = await fetch(url +'/'+ id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const response = await json.json();
        console.log(response);

    } catch (error) {
       alert(error);
    }
}
}
